import "./commands";
import { Root as Api } from "../../api-types/types";

declare global {
  namespace Cypress {
    interface Chainable {
      verifyGqlResponse(node: Api);
    }
  }
}
