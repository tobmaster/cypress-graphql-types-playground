To use this playground

```
npm install
````

Generate the Typescript Types based on StarWars GraphQL API 

```
npm run generate
````
